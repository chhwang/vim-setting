set softtabstop=4
set tabstop=4
set shiftwidth=4
set expandtab
set mouse=a 
set nu
set ai
set si
set hlsearch
set showmatch
set wmnu
set autochdir
syntax enable
colorscheme monokai
set backspace=indent,eol,start
au BufNewFile,BufRead *.cu set filetype=cuda
au BufNewFile,BufRead *.cuh set filetype=cuda
set formatoptions=l
set lbr
